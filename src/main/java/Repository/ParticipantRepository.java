package Repository;

import Domain.Participant;

import java.sql.*;
import java.util.ArrayList;

public class ParticipantRepository implements Repository<Integer, Participant> {
    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    String url;

    public ParticipantRepository() throws ClassNotFoundException {
        connection = null;
        statement = null;
        preparedStatement = null;
        resultSet = null;
        url = "jdbc:mysql://localhost/motorsport?user=sqluser&password=sqluserpw";
        Class.forName("com.mysql.cj.jdbc.Driver");
    }

    @Override
    public void createConnection() throws SQLException {
        connection = DriverManager.getConnection(url);
    }

    @Override
    public void endConnection() throws SQLException {
        connection.close();
    }

    @Override
    public Participant findOne(Integer integer) throws ClassNotFoundException, SQLException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from Participant where pid =" + integer);

        Participant participant;
        resultSet.first();

        participant = new Participant(resultSet.getInt("pid"), resultSet.getString("name"),
                resultSet.getInt("engineCapacity"), resultSet.getInt("tid"));

        endConnection();
        return participant;
    }

    @Override
    public Iterable<Participant> findAll() throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from Participant");

        Participant participant;
        ArrayList<Participant> participants = new ArrayList<>();

        while (resultSet.next()) {
            participant = new Participant(resultSet.getInt("pid"), resultSet.getString("name"),
                    resultSet.getInt("engineCapacity"), resultSet.getInt("tid"));
            participants.add(participant);
        }

        endConnection();

        return participants;
    }

    @Override
    public Participant save(Participant entity) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();


        String sql = "INSERT INTO Participant (name, tid, engineCapacity) VALUES (" + entity.toSQL() + ");";
        System.out.println(sql);
        if (statement.execute(sql)) {
            endConnection();

            return null;
        }


        endConnection();

        return entity;
    }

    @Override
    public Participant delete(Integer integer) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();


        String sql = "DELETE FROM Participant WHERE pid=" + integer + ";";

        boolean result = statement.execute(sql);

        endConnection();

        return null;
    }

    @Override
    public Participant update(Participant entity) throws SQLException {
        createConnection();
        statement = connection.createStatement();

        String sql = "UPDATE Participant SET name=" + "'" + entity.getName()+ "' WHERE pid=" +
                entity.getId() + ";";
        statement.execute(sql);

        endConnection();

        return null;
    }
}
