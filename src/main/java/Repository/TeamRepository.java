package Repository;

import Domain.Team;

import java.sql.*;
import java.util.ArrayList;

public class TeamRepository implements Repository<Integer, Team>{
    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    String url;

    public TeamRepository() throws ClassNotFoundException {
        connection = null;
        statement = null;
        preparedStatement = null;
        resultSet = null;
        url = "jdbc:mysql://localhost/motorsport?user=sqluser&password=sqluserpw";
        Class.forName("com.mysql.cj.jdbc.Driver");
    }

    @Override
    public void createConnection() throws SQLException {
        connection = DriverManager.getConnection(url);
    }

    @Override
    public void endConnection() throws SQLException {
        connection.close();
    }

    @Override
    public Team findOne(Integer integer) throws ClassNotFoundException, SQLException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from Team where tid =" + integer);

        Team team;
        resultSet.first();

        team = new Team(resultSet.getInt("tid"), resultSet.getString("name"));

        endConnection();
        return team;
    }

    @Override
    public Iterable<Team> findAll() throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from Team");

        Team team;
        ArrayList<Team> teams = new ArrayList<>();

        while (resultSet.next()) {
            team = new Team(resultSet.getInt("tid"), resultSet.getString("name"));
            teams.add(team);
        }

        endConnection();

        return teams;
    }

    @Override
    public Team save(Team entity) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();

        String sql = "INSERT INTO Team (name) VALUES (" + "'" +entity.toSQL() + "'" + ");";

        if (statement.execute(sql)) {
            endConnection();

            return null;
        }


        endConnection();

        return entity;
    }

    @Override
    public Team delete(Integer integer) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();


        String sql = "DELETE FROM Team WHERE tid=" + integer + ";";

        boolean result = statement.execute(sql);

        endConnection();

        return null;
    }

    @Override
    public Team update(Team entity) throws SQLException {
        createConnection();
        statement = connection.createStatement();
        String sql = "UPDATE Team SET name=" + "'" + entity.getName() + "'" + " WHERE tid=" +
                entity.getId() + ";";
        statement.execute(sql);

        endConnection();

        return null;
    }
}
