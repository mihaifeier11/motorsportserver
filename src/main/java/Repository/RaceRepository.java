package Repository;

import Domain.Race;

import java.sql.*;
import java.util.ArrayList;

public class RaceRepository implements Repository<Integer, Race> {
    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    String url;

    public RaceRepository() throws ClassNotFoundException {
        connection = null;
        statement = null;
        preparedStatement = null;
        resultSet = null;
        url = "jdbc:mysql://localhost/motorsport?user=sqluser&password=sqluserpw";
        Class.forName("com.mysql.cj.jdbc.Driver");
    }

    @Override
    public void createConnection() throws SQLException {
        connection = DriverManager.getConnection(url);
    }

    @Override
    public void endConnection() throws SQLException {
        connection.close();
    }

    @Override
    public Race findOne(Integer s) throws SQLException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from motorsport.races where id =" + s);

        Race race;
        resultSet.first();

        race = new Race(resultSet.getInt("id"), resultSet.getInt("engineCapacity"),
                resultSet.getInt("nrParticipants"));

        endConnection();
        return race;
    }

    @Override
    public Iterable<Race> findAll() throws SQLException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from Races");

        Race race;
        ArrayList<Race> races = new ArrayList<>();

        while (resultSet.next()) {
            race = new Race(resultSet.getInt("rid"), resultSet.getInt("engineCapacity"),
                    resultSet.getInt("nrParticipants"));
            races.add(race);
        }

        endConnection();

        return races;
    }

    @Override
    public Race save(Race entity) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();

        String sql = "INSERT INTO Races (engineCapacity, nrParticipants) VALUES (" + entity.toSQL() + ");";

        if (statement.execute(sql)) {
            endConnection();

            return null;
        }


        endConnection();

        return entity;
    }

    @Override
    public Race delete(Integer s) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();


        String sql = "DELETE FROM Races WHERE rid=" + s + ";";

        boolean result = statement.execute(sql);

        endConnection();

        return null;
    }

    @Override
    public Race update(Race entity) throws SQLException {
        createConnection();
        statement = connection.createStatement();
        String sql = "UPDATE Races SET engineCapacity=" + entity.getEngineCapacity() +
                ", nrParticipants=" + entity.getNrOfParticipants() + " WHERE rid=" +
                entity.getId() + ";";
        statement.execute(sql);

        endConnection();

        return null;
    }
}
