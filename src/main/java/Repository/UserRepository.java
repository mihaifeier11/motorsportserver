package Repository;

import Domain.User;

import java.sql.*;
import java.util.ArrayList;

public class UserRepository implements Repository<Integer, User> {
    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    String url;

    public UserRepository() throws ClassNotFoundException {
        connection = null;
        statement = null;
        preparedStatement = null;
        resultSet = null;
        url = "jdbc:mysql://localhost/motorsport?user=sqluser&password=sqluserpw";
        Class.forName("com.mysql.cj.jdbc.Driver");
    }

    @Override
    public void createConnection() throws SQLException {
        connection = DriverManager.getConnection(url);
    }

    @Override
    public void endConnection() throws SQLException {
        connection.close();
    }

    public boolean login(String username, String password) throws SQLException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from User where name ='" + username + "' and passwd='" + password + "';");

        return resultSet.first();
    }

    @Override
    public User findOne(Integer integer) throws ClassNotFoundException, SQLException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from User where uid =" + integer);

        User user;
        resultSet.first();

        user = new User(resultSet.getInt("uid"), resultSet.getString("name"),
                resultSet.getString("passwd"));

        endConnection();
        return user;
    }

    @Override
    public Iterable<User> findAll() throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from User");

        User user;
        ArrayList<User> users = new ArrayList<>();

        while (resultSet.next()) {
            user = new User(resultSet.getInt("uid"), resultSet.getString("name"),
                    resultSet.getString("passwd"));
            users.add(user);
        }

        endConnection();

        return users;
    }

    @Override
    public User save(User entity) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();


        String sql = "INSERT INTO User (name, passwd) VALUES (" + entity.toSQL() + ");";

        if (statement.execute(sql)) {
            endConnection();

            return null;
        }


        endConnection();

        return entity;
    }

    @Override
    public User delete(Integer integer) throws SQLException, ClassNotFoundException {
        createConnection();

        statement = connection.createStatement();


        String sql = "DELETE FROM User WHERE uid=" + integer + ";";

        boolean result = statement.execute(sql);

        endConnection();

        return null;
    }

    @Override
    public User update(User entity) throws SQLException {
        createConnection();
        statement = connection.createStatement();

        String sql = "UPDATE User SET name=" + "'" + entity.getName() +
                "', passwd=" + "'" + entity.getPassword() + "' WHERE uid=" +
                entity.getId() + ";";
        statement.execute(sql);

        endConnection();

        return null;
    }
}
