import Controller.AbstractServer;
import Controller.Controller;
import Controller.SequentialServer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.startServer();

    }
}
