package Controller;

import Repository.UserRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Date;

public class SequentialServer extends AbstractServer {
    public SequentialServer(int port) {
        super(port);
        System.out.println("SequentialServer");
    }

    protected void processRequest(Socket client) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
             PrintWriter writer = new PrintWriter(client.getOutputStream())) {

            //read message from client
            String user = br.readLine();
            String password = br.readLine();
            UserRepository userRepository = new UserRepository();
            boolean result = userRepository.login(user, password);
            if (result) {
                writer.write(0);
            } else {
                writer.write(1);
            }
            writer.flush();

        } catch (IOException e) {
            System.err.println("Communication error " + e);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}