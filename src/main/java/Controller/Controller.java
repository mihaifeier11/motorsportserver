package Controller;


public class Controller {
    public Controller() {

    }

    public void startServer() {
        AbstractServer server = new SequentialServer(55554);
        try {
            server.start();
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }
}
